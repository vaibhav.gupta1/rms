import {Component, OnInit} from '@angular/core';
import {AuthService, GoogleLoginProvider} from 'angularx-social-login';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
  }

  signInWithGoogle() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then(user => {
        // if signIn success
        console.log(user);

        // storing user data in session storage
        sessionStorage.setItem('user', JSON.stringify(user));
        this.router.navigate(['/dashboard']);
      });
  }

}
