import {Component, OnInit} from '@angular/core';
import {AuthService, SocialUser} from 'angularx-social-login';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private user: SocialUser;

  constructor(private router: Router, private authService: AuthService) {
  }

  ngOnInit() {
    if (sessionStorage.length > 0) {
      this.user = JSON.parse(sessionStorage.getItem('user')) as SocialUser;
      console.log(this.user);
    } else {
      this.router.navigate(['/login']);
    }
  }

  logoutUser() {
    this.authService.signOut().then(data => {
      sessionStorage.clear();
      this.router.navigate(['/login']);
    });
  }
}
